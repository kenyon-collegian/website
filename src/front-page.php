<?php

$context = Timber::get_context();
$context['page'] = new Timber\Post();
$context['post_ids'] = kc_front_page_post_ids();
$templates = array( 'front-page.twig', 'page.twig', 'index.twig' );
Timber::render( $templates, $context );

<?php
/**
 * The template for displaying Author Archive pages
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */
global $wp_query;
global $paged;
if (!isset($paged) || !$paged){
	$paged = 1;
}

$args = array(
	'paged' => $paged
);
$context = Timber::get_context();
if ( isset( $wp_query->query_vars['author'] ) ) {
	$post_ids = custom_author_post_ids( $wp_query->query_vars['author'] );
	$author = new TimberUser( $wp_query->query_vars['author'] );
	$context['author'] = $author;
	$context['title'] = $author->name();
}

if ( isset( $post_ids ) ) {
	$args['post__in'] = $post_ids;
}

$context['paged'] = $paged;
$context['posts'] = new Timber\PostQuery( $args );
Timber::render( array( 'author.twig', 'archive.twig' ), $context );

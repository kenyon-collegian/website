<?php

require_once(__DIR__ . "/includes/ads.php");

acf_add_options_page( array(
	'page_title' => 'Options',
	'capability' => 'edit_users'
));

add_action( 'after_setup_theme', function() {
	add_theme_support( 'post-formats', array( 'image' ) );
} );

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
		echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php') ) . '</a></p></div>';
	});

	add_filter('template_include', function($template) {
		return get_stylesheet_directory() . '/static/no-timber.html';
	});

	return;
}

add_action( 'template_redirect', 'kc_attempt_redirect_to_migrated_post' );
function kc_attempt_redirect_to_migrated_post( ) {
	$old_permalink_pattern = '~^\/([\d]{4})\/([\d]{2})\/([\d]{2})\/(?!page\/)(?<slug>.+)\/?$~';
	$request_match = preg_match( $old_permalink_pattern, $_SERVER['REQUEST_URI'], $uri_components );

	// Bail if this is not a 404 page, or if the request URI is not for a migrated post
	if ( !is_404() || $request_match !== 1 ) return;

	// Find the migrated post
	// Bail if no posts found
	$matching_posts = get_posts( array(
		'name' => $uri_components['slug'],
		'post_type' => 'post',
		'post_status' => 'publish',
		'fields' => 'ids',
		'numberposts' => 1
	));
	if (!$matching_posts) return;

	// Redirect to the migrated post
	$post_id = $matching_posts[0];
	$permalink = get_permalink( $post_id );
	if ( !$permalink ) return;
	wp_redirect( $permalink, 301 );
	exit;
}

function kc_custom_authors_where( $where ) {
	global $wpdb;
	$where = str_replace(
		"meta_key = 'authors_%",
		"meta_key LIKE 'authors_%",
		$wpdb->remove_placeholder_escape($where)
	);
	return $where;
}
add_filter( 'posts_where', 'kc_custom_authors_where');

// Dealing with breaking news alerts
add_action( 'save_post', 'invalidate_cached_breaking_news', 10, 3 );
function invalidate_cached_breaking_news( $post_ID, $post, $update ) {
	$post_type = get_post_type($post_ID);
	if ( 'kc_breaking_news' != $post_type ) return;

	delete_transient( 'kc_latest_breaking_news_alert' );
	delete_transient( 'kc_front_page_post_ids' );
}

// invalidate the front page pid cache
add_action( 'save_post', 'invalidate_cached_front_page_pids', 10, 3 );
function invalidate_cached_front_page_pids( $post_ID, $post, $update ) {
	if ( 'post' != get_post_type($post_ID) || 'publish' != $post->post_status ) return;
	delete_transient( 'kc_front_page_post_ids' );
}

function latest_breaking_news_alert() {
	if ( false === get_transient( 'kc_latest_breaking_news_alert' ) ) {
		$args = array(
			'post_type' => 'kc_breaking_news',
			'date_query' => array(
				'column' => 'post_modified',
				'after' => '1 day ago'
			),
			'posts_per_page' => 1
		);
		$alerts = get_posts( $args );
		$alert = $alerts[0] ?? 0;
		set_transient( 'kc_latest_breaking_news_alert', $alert, 12 * HOUR_IN_SECONDS );
	}
	return get_transient( 'kc_latest_breaking_news_alert' );
}

function latest_breaking_news_article() {
	if ( $latest_alert = latest_breaking_news_alert() ) {
		return get_post_meta($latest_alert->ID, 'follow_up_post', true);
	}
	return false;
}

// Special queries for category pages
function latest_featured_in_category( $cat_ID, $number = 6, $not = array() ) {
	$args = array(
		'cat' => $cat_ID,
		'meta_key' => 'featured',
		'meta_value' => 1,
		'posts_per_page' => $number,
		'fields' => 'ids'
	);
	$breaking_news_article = latest_breaking_news_article();

	if ( sizeof($not) ) {
		$args['post__not_in'] = $not;
	}

	$featured_ids = get_posts( $args );

	// A breaking news article in this category can supersede the stack
	// of featured articles
	if ( $breaking_news_article != false ) {
		$bn_cats = array_map( get_the_category( $breaking_news_article ), function( $term ) {
			return $term->term_id;
		});

		if ( array_search( $cat_ID, $bn_cats ) ) {
			array_pop( $featured_ids ) && array_unshift( $breaking_news_article );
		}
	}

	return $featured_ids;
}

function custom_author_post_ids( $author_ID ) {
	// Get the IDs of all posts with the author set through the WP
	// default configuration, but only on the posts that haven't
	// used the new ACF custom byline fields
	$legacy_query = new WP_Query(array(
		'author' => $author_ID,
		'meta_key' => 'authors',
		'meta_compare' => 'NOT EXISTS',
		'numberposts' => -1,
		'fields' => 'ids'
	));

	// Get the IDs of all posts with the author set through the new
	// ACF configuration.
	$custom_query = new WP_Query(array(
		'meta_query' => array(
			array(
				'key' => 'authors_%_staff_author',
				'value' => $author_ID,
				'compare' => '='
			)
		),
		'numberposts' => -1,
		'fields' => 'ids'
	));


	$pids = array_unique( array_merge(
		$legacy_query->posts,
		$custom_query->posts
	) );
	return $pids;
}

function kc_front_page_post_ids() {
	if (false === get_transient('kc_front_page_post_ids')) {
		$pid_table = array();

		// Get the top story, either the latest breaking news story
		// or the latest featured news story w/ an image
		if ( latest_breaking_news_article() ) {
			$pid_table['top_story'] = latest_breaking_news_article();
		} else {
			$top_story_query = new WP_Query( array(
				'category_name' => 'news',
				'meta_query' => array(
					'relation' => 'AND',
					array(
						'key' => '_thumbnail_id',
						'compare' => 'EXISTS'
					),
					array(
						'key' => 'featured',
						'value' => '1',
						'compare' => '='
					)
				),
				'numberposts' => 1,
				'fields' => 'ids'
			) );
			$pid_table['top_story'] = $top_story_query->posts[0];
		}

		// Get the four latest featured stories with featured images (excluding the previous story and opinions)
		$latest_featured_query = new WP_Query( array(
			'post__not_in' => array($pid_table['top_story']),
			'tax_query' => array(
				array(
					'taxonomy' => 'category',
					'field' => 'slug',
					'terms' => 'opinion',
					'operator' => 'NOT IN'
				),
			),
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'key' => 'featured',
					'value' => '1'
				),
				array(
					'key' => '_thumbnail_id',
					'compare' => 'EXISTS'
				)
			),
			'numberposts' => 4,
			'fields' => 'ids'
		) );
		$pid_table['latest_featured'] = $latest_featured_query->posts;

		// Get the 6 latest news stories (excluding the previous stories)
		$latest_news_query = new WP_Query( array(
			'post__not_in' => array_merge(
				array($pid_table['top_story']),
				$pid_table['latest_featured']
			),
			'category_name' => 'news',
			'numberposts' => 7,
			'fields' => 'ids'
		) );
		$pid_table['latest_news'] = $latest_news_query->posts;

		// Get the five latest stories overall (excluding previous stories and opinions)
		// The first story must have an image
		$latest_w_image_query = new WP_Query( array(
			'post__not_in' => array_merge(
				array($pid_table['top_story']),
				$pid_table['latest_news'],
				$pid_table['latest_featured']
			),
			'tax_query' => array(
				array(
					'taxonomy' => 'category',
					'field' => 'slug',
					'terms' => 'opinion',
					'operator' => 'NOT IN'
				),
			),
			'meta_query' => array(
				array(
					'key' => '_thumbnail_id',
					'compare' => 'EXISTS'
				)
			),
			'numberposts' => 1,
			'fields' => 'ids'
		) );
		$latest_any_query = new WP_Query( array(
			'post__not_in' => array_merge(
				array($pid_table['top_story']),
				$pid_table['latest_news'],
				$pid_table['latest_featured'],
				$latest_w_image_query->posts
			),
			'tax_query' => array(
				array(
					'taxonomy' => 'category',
					'field' => 'slug',
					'terms' => 'opinion',
					'operator' => 'NOT IN'
				),
			),
			'numberposts' => 3,
			'fields' => 'ids'
		) );
		$pid_table['latest_any'] = array_merge(
			$latest_w_image_query->posts,
			$latest_any_query->posts
		);

		// Get the latest editorial
		$editorial_query = new WP_Query( array(
			'category_name' => 'editorial',
			'numberposts' => 1,
			'fields' => 'ids'
		) );
		$pid_table['editorial'] = $editorial_query->posts[0];

		// Get the two latest opinion stories (not cartoons or editorials)
		$op_eds_query = new WP_Query( array(
			'tax_query' => array(
				'relation' => 'AND',
				array(
					'taxonomy' => 'category',
					'field' => 'slug',
					'terms' => array( 'opinion' )
				),
				array(
					'taxonomy' => 'category',
					'field' => 'slug',
					'terms' => array( 'editorial', 'cartoon' ),
					'operator' => 'NOT IN'
				)
			),
			'numberposts' => 2,
			'fields' => 'ids'
		) );
		$pid_table['op_eds'] = $op_eds_query->posts;

		// get the five most recent stories in each of the four
		// non-news sections.
		$sections = array( 'features', 'arts', 'opinion', 'sports' );
		foreach($sections as $section) {
			$section_query = new WP_Query( array(
				'category_name' => $section,
				'numberposts' => 5,
				'fields' => 'ids'
			) );
			$pid_table[ 'latest_' . $section ] = $section_query->posts;
		}

		set_transient( 'kc_front_page_post_ids', $pid_table, WEEK_IN_SECONDS );
	}
	return get_transient( 'kc_front_page_post_ids' );
}

Timber::$dirname = array('templates', 'views');

class StarterSite extends TimberSite {

	function __construct() {
		add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'get_twig', array( $this, 'add_to_twig' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'register_taxonomies' ) );
		parent::__construct();
	}

	function register_post_types() {
		//this is where you can register custom post types
		$labels = array(
			'name'                  => _x( 'Breaking News Alerts', 'Post Type General Name', 'text_domain' ),
			'singular_name'         => _x( 'Breaking News Alert', 'Post Type Singular Name', 'text_domain' ),
			'menu_name'             => __( 'Breaking News', 'text_domain' ),
			'name_admin_bar'        => __( 'Breaking News', 'text_domain' ),
			'archives'              => __( 'Breaking News Archives', 'text_domain' ),
			'attributes'            => __( 'Alert Attributes', 'text_domain' ),
			'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
			'all_items'             => __( 'All Alerts', 'text_domain' ),
			'add_new_item'          => __( 'Add New Alert', 'text_domain' ),
			'add_new'               => __( 'Add New Alert', 'text_domain' ),
			'new_item'              => __( 'New Alert', 'text_domain' ),
			'edit_item'             => __( 'Edit Alert', 'text_domain' ),
			'update_item'           => __( 'Update Alert', 'text_domain' ),
			'view_item'             => __( 'View Alert', 'text_domain' ),
			'view_items'            => __( 'View Alerts', 'text_domain' ),
			'search_items'          => __( 'Search Alert', 'text_domain' ),
			'not_found'             => __( 'No alerts found', 'text_domain' ),
			'not_found_in_trash'    => __( 'No alerts found in Trash', 'text_domain' ),
			'featured_image'        => __( 'Featured Image', 'text_domain' ),
			'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
			'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
			'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
			'insert_into_item'      => __( 'Insert into alert', 'text_domain' ),
			'uploaded_to_this_item' => __( 'Uploaded to this alert', 'text_domain' ),
			'items_list'            => __( 'Alerts list', 'text_domain' ),
			'items_list_navigation' => __( 'Alerts list navigation', 'text_domain' ),
			'filter_items_list'     => __( 'Filter alerts list', 'text_domain' ),
		);
		$args = array(
			'label'                 => __( 'Breaking News Alert', 'text_domain' ),
			'description'           => __( 'A sitewide banner alert about breaking news', 'text_domain' ),
			'labels'                => $labels,
			'supports'              => array( 'title', 'editor', 'revisions', 'custom-fields', ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'menu_icon'             => 'dashicons-warning',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => false,
			'can_export'            => true,
			'has_archive'           => false,
			'exclude_from_search'   => true,
			'publicly_queryable'    => false,
			'capability_type'       => 'post',
			'show_in_rest'          => true,
		);
		register_post_type( 'kc_breaking_news', $args );
	}

	function register_taxonomies() {
		//this is where you can register custom taxonomies
	}

	function add_to_context( $context ) {
		if ($breaking_news_alert = latest_breaking_news_alert()) {
			$context['breaking_news_alert'] = new Timber\Post($breaking_news_alert->ID);
		}
		if ($breaking_news_article = latest_breaking_news_article()) {
			$context['breaking_news_article'] = new Timber\Post($breaking_news_article);
		}
		$context['options'] = get_fields('options');
		$context['menu'] = new TimberMenu('Header');
		$context['secondary_menu'] = new TimberMenu('Header Secondary');
		$context['footer_menu'] = new TimberMenu('Footer');
		$context['site'] = $this;
		return $context;
	}

	function kc_format_relative_timestamp( $iso_date_string ) {
		$today = new DateTime();
		$target_date = new DateTime( $iso_date_string );
		$interval = date_diff($today, $target_date);

		if ( $interval->days ) {
			return $target_date->format( 'F j' );
		} elseif ( $interval->h >= 1 ) {
			return $interval->format('%hh ago');
		} else {
			return $interval->format( '%im ago' );
		}
	}

	function add_to_twig( $twig ) {
		/* this is where you can add your own functions to twig */
		$twig->addExtension( new Twig_Extension_StringLoader() );
		$twig->addFilter('format_alert_timestamp', new Twig_SimpleFilter('format_alert_timestamp', array($this, 'kc_format_alert_timestamp')));
		$twig->addFilter('format_relative_timestamp', new Twig_SimpleFilter('format_relative_timestamp', array($this, 'kc_format_relative_timestamp')));
		return $twig;
	}
}

add_filter( 'timber/twig', array( AdRotateEnhancements::class, 'register' ) );

function kc_enqueue_theme_files() {
	wp_enqueue_script( 'main', get_template_directory_uri() . '/assets/main.js', array(), filemtime(get_template_directory() . '/assets/main.js'), true);
	wp_enqueue_script( 'icons', get_template_directory_uri() . '/assets/icons.js', array(), filemtime(get_template_directory() . '/assets/icons.js'), true);
	wp_enqueue_style( 'screen', get_template_directory_uri() .'/style.css', array(), filemtime(get_template_directory() . '/style.css'));
}
add_action( 'wp_enqueue_scripts', 'kc_enqueue_theme_files' );

function kc_defer_icons($tag, $handle) {
	if ( $handle != 'icons' ) return $tag;
	return str_replace( ' src', ' async defer src', $tag );
}

add_filter('script_loader_tag', 'kc_defer_icons', 10, 2);

add_filter( 'embed_oembed_html', 'wrap_oembed_html', 99, 4 );

function wrap_oembed_html( $cached_html, $url, $attr, $post_id ) {
    if ( false !== strpos( $url, "://youtube.com") || false !== strpos( $url, "://youtu.be" ) ) {
        $cached_html = '<div class="c-article__resp-video">' . $cached_html . '</div>';
    }
    return $cached_html;
}

new StarterSite();

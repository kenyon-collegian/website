const headroom = require('headroom.js');
const Toggle = require('./behaviors/toggle');
const Loader = require('./behaviors/loader');
const CommentFormController = require('./components/comment-forms');

var toggles = {};
var toggleEls = document.querySelectorAll('.js-toggle');
for (let j = 0; j < toggleEls.length; j++) {
  let el = toggleEls[j];
  let tog = new Toggle(el);
  toggles[el.id] = tog;
  el.addEventListener('click', function() {
    tog.toggle();
  });
}
  

var loaderEls = document.querySelectorAll('.js-loader');
for (let k = 0; k < loaderEls.length; k++) {
  let el = loaderEls[k];
  let loader = new Loader(el);
  el.addEventListener('click', function() {
    loader.load();
  })
}

var commentForms = CommentFormController();
var commentToggles = document.querySelectorAll('.js-cmt-toggle');
for (let m = 0; m < commentToggles.length; m++) {
  commentForms.addToggle(commentToggles[m]);
}

window.addEventListener('keyup', function(e) {
  if (e.which === 27) {
    toggles['search-toggle'].close();
    toggles['menu-toggle'].close();
  }
}, true);

const headroomClasses = {
  initial: 'is-mounted',
  pinned: 'is-pinned',
  unpinned: 'is-unpinned',
  top: 'is-top',
  notTop: 'is-not-top',
  bottom: 'is-bottom',
  notBottom: 'is-not-bottom'
};
const headerEl = document.querySelector('#header');
const shareToolsEl = document.querySelector('#share-tools');
const header = new headroom(headerEl, {
  offset: 64,
  tolerance: 10,
  classes: headroomClasses,
  onUnpin: function() {
    toggles['search-toggle'].close()
  }
});
header.init();

if (shareToolsEl) {
  const shareTools = new headroom(shareToolsEl, {
    tolerance: 10,
    classes: headroomClasses
  });
  shareTools.init();
}

const feather = require('feather-icons');

feather.replace({
  class: 'c-icon',
  'aria-hidden': 'true'
});

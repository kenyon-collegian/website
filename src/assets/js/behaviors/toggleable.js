Toggleable.prototype.isClosed = function() {
    return !!this.el.getAttribute('aria-hidden');
}

Toggleable.prototype.open = function() {
    this.el.classList.remove('is-closed');
    this.el.removeAttribute('aria-hidden');
}

Toggleable.prototype.close = function() {
    this.el.classList.add('is-closed');
    this.el.setAttribute('aria-hidden', 'true');
}

Toggleable.prototype.init = function() {};

function Toggleable(el) {
    this.el = el;
}

module.exports = Toggleable;

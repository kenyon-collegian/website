const Toggleable = require('./toggleable');
const Dismissable = require('./dismissable');
const Alert = require('./alert');

function Toggle(toggle) {
    let targetEl = document.getElementById(toggle.getAttribute('data-target'));
    this.flags = {
        open: 'has-' + targetEl.id + '-open',
        close: 'has-' + targetEl.id + '-closed'
    };
    this.control = toggle;
    this.target = targetEl.classList.contains('js-dismissable') ? new Dismissable(targetEl) : new Toggleable(targetEl);
    this.target.init();

    let alert = document.getElementById(toggle.getAttribute('data-alert'))
    if (alert) {
        this.alert = new Alert(alert, { 'open': 'is open', 'closed': 'is closed' });
    }
}

Toggle.prototype.open = function() {
    this.control.classList.remove('is-closed');
    document.documentElement.classList.add(this.flags.open);
    document.documentElement.classList.remove(this.flags.close);
    this.target.open();
    this.alert && this.alert.notify('open');
}

Toggle.prototype.close = function() {
    this.control.classList.add('is-closed');
    document.documentElement.classList.add(this.flags.close);
    document.documentElement.classList.remove(this.flags.open);
    this.target.close();
    this.alert && this.alert.notify('closed');
}
    
Toggle.prototype.toggle = function() {
    if (this.target.isClosed()) {
        this.open();        
    } else {
        this.close();
    }

}

module.exports = Toggle;

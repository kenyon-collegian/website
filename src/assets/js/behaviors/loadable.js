Loadable.prototype.load = function(url, onload, onerror) {
    const _this = this;
    asyncLoadPage(url).then( function(nextPage) {
        let newList = nextPage.querySelector('[data-channel="' + _this.channel + '"]');
        let dest = newList.querySelector('a, button, [tabindex]');
        let newLoader = nextPage.querySelector('[data-target="' + _this.el.id + '"]');
        let newNext = newLoader && newLoader.getAttribute('data-next');
        var count = 0;

        _this.el.classList.add('is-locked');
        while (newList.children.length > 0) {
            count++;
            newList.children[0].style.animationDelay = '' + (count * 120) + 'ms';
            _this.el.appendChild(newList.children[0]);
        }
        _this.el.classList.remove('is-locked');

        onload.call(_this.loader, dest, newNext);
    }, function(status) {
        onerror.call(_this.loader);
    });
}

function Loadable(el, loader) {
    this.el = el;
    this.loader = loader;
    this.channel = el.getAttribute('data-channel');
}

function asyncLoadPage(url) {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.open("GET", url);
        xhr.responseType = 'document';
        xhr.onload = () => resolve(xhr.responseXML);
        xhr.onerror = () => reject(xhr.statusText);
        xhr.send();
    });
}

module.exports = Loadable;

// Manages the state of an [aria-role="alert"] element
Alert.prototype.notify = function(state) {
    this.field.innerText = [this.label, this.states[state]].join(' ');
}

function Alert(alert, states = {}) {
    this.field = alert;
    this.states = states;
    this.label = alert.getAttribute('data-label');
}

module.exports = Alert;

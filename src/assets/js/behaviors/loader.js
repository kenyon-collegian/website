const Loadable = require('./loadable');
const Alert = require ('./alert');

Loader.prototype.load = function() {
    if (this.next) {
        this.alert && this.alert.notify('loading');
        this.el.setAttribute('disabled', '');
        this.el.classList.remove('is-errored');
        this.el.classList.add('is-loading');
        this.target.load(this.next, this.handleSuccess, this.handleError);
    } else {
        this.handleError();
    }
}

Loader.prototype.handleError = function() {
    this.alert && this.alert.notify('error');
    this.el.classList.remove('is-loading');
    this.el.classList.add('is-errored');
    this.el.removeAttribute('disabled');
}

Loader.prototype.handleSuccess = function(dest, next = false) {
    this.alert && this.alert.notify('loaded');
    this.el.classList.remove('is-loading');
    dest.focus();
    this.next = next;
    if (next) {
        this.el.removeAttribute('disabled');
    } else {
        this.alert && this.alert.notify('noNext');
        this.vanish();
    }
}

Loader.prototype.vanish = function() {
    this.el.setAttribute('disabled', '');
    this.el.classList.add('is-hidden');
}

function Loader(el) {
    let targetID = el.getAttribute('data-target');
    let targetEl = document.getElementById(targetID);
    let alertID = el.getAttribute('data-alert');
    let alertEl = document.getElementById(alertID);

    this.el = el;
    this.next = el.getAttribute('data-next');

    if (!targetEl) {
        this.vanish();
    } else {
        this.target = new Loadable(targetEl, this);
    }

    if (alertEl) {
        this.alert = new Alert(alertEl, {
            'loading': 'Loading more articles',
            'loaded': 'Articles loaded',
            'error': 'Error loading articles',
            'noNext': 'End of articles'
        })
    }
}

module.exports = Loader;

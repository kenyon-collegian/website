const Toggleable = require('./toggleable');

Dismissable.prototype = Object.create(Toggleable.prototype);
Dismissable.constructor = Dismissable;

Dismissable.prototype.openOnStateChange = function(lastModified, stateData) {
  if (!stateData['dismissed'] || lastModified > stateData['lastModified']) {
    this.open();
  }
}

Dismissable.prototype.saveState = function(label, dismissed, lastModified) {
  let stateData = JSON.stringify({
    'dismissed': dismissed,
    'lastModified': lastModified
  })
  window.localStorage.setItem(label, stateData);
}

Dismissable.prototype.close = function() {
  Toggleable.prototype.close.call(this);
  this.saveState(this.label, true, this.lastModified);
}

Dismissable.prototype.init = function() {
  if (!window.localStorage) {
    
    this.open();
  
  } else {

    if (window.localStorage.getItem(this.label)) {
  
      let stateData = JSON.parse(window.localStorage.getItem(this.label));
      this.openOnStateChange(this.lastModified, stateData);
  
    } else {
  
      this.saveState(this.label, false, this.lastModified);
      this.open();
      
    }

  }
}

function Dismissable(el) {
  Toggleable.call(this, el);
  this.lastModified = Date.parse(el.getAttribute('data-modified-datetime'));
  this.label = el.id;
}

module.exports = Dismissable;

const ToggleFactory = (el) => ({
    notify (state) {
        if (state === 'closed') {
            el.classList.remove('is-open');
            el.classList.add('is-closed');
        } else {
            el.classList.remove('is-closed');
            el.classList.add('is-open');
        }
    }
})

const CommentFormFactory = (el) => ({
    isClosed: !!el.getAttribute('aria-hidden'),
    toggles: [],
    toggle () {
        if (this.isClosed) {
            el.classList.remove('is-closed');
            el.classList.add('is-open');            
            el.removeAttribute('aria-hidden');
            this.isClosed = false;
        } else {
            el.classList.remove('is-open');
            el.classList.add('is-closed');
            el.setAttribute('aria-hidden', 'true');
            this.isClosed = true;
        }
        for (let i = 0; i < this.toggles.length; i++) {
            this.toggles[i].notify( this.isClosed ? 'closed' : 'open' );
        }
    },
    registerToggle (toggle) {
        this.toggles.push(toggle);
    }
})

const CommentFormController = () => ({
    forms: {},
    addToggle (toggleEl) {
        let targetId = toggleEl.getAttribute('data-target');
        if (this.forms[targetId] === undefined) {
            this.forms[targetId] = CommentFormFactory(document.getElementById(targetId));
        }
        this.forms[targetId].registerToggle(ToggleFactory(toggleEl));
        toggleEl.addEventListener('click', () => { this.forms[targetId].toggle() });
    }
})

module.exports = CommentFormController;

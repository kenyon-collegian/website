<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */
 global $paged;

$templates = array( 'archive.twig', 'index.twig' );

$context = Timber::get_context();
$query_args = array();

$context['title'] = 'The Latest';
if ( is_day() ) {
	$context['title'] = 'Articles from '.get_the_date( 'D M Y' );
} else if ( is_month() ) {
	$context['title'] = 'Articles from '.get_the_date( 'M Y' );
} else if ( is_year() ) {
	$context['title'] = 'Articles from '.get_the_date( 'Y' );
} else if ( is_tag() ) {
	$context['title'] = single_tag_title( '', false );
	array_unshift( $templates, 'archive-tag.twig' );
} else if ( is_category() ) {
	$context['title'] = single_cat_title( '', false );
	$context['category'] = new Timber\Term();

	if ( ($termID = $context['category']->parent) ) {
		$context['parent_cat'] = new Timber\Term($termID);
	}

	array_unshift( $templates, 'archive-category.twig' );
	array_unshift( $templates, 'archive-' . get_query_var( 'cat' ) . '.twig' );
} else if ( is_post_type_archive() ) {
	$context['title'] = post_type_archive_title( '', false );
	array_unshift( $templates, 'archive-' . get_post_type() . '.twig' );
}

$context['posts'] = new Timber\PostQuery();
$context['paged'] = $paged;

Timber::render( $templates, $context );

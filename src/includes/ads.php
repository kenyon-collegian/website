<?php

class AdRotateEnhancements {
	static function register( $twig ) {
		$enhancements = new AdRotateEnhancements();

		$twig->addFunction( new Timber\Twig_Function(
			'has_ads_for_group',
			array( $enhancements, 'has_ads_for_group' )
		) );

		$twig->addFunction( new Timber\Twig_Function(
			'ad_group',
			array( $enhancements, 'ad_group' )
		) );

		return $twig;
	}

	function has_ads_for_group ( $id ) {
		if ( ! $this->is_adrotate_enabled() ) return FALSE;

		global $wpdb;

		return (bool)$wpdb->get_var(
			$wpdb->prepare(
				"
					SELECT 1
					FROM {$wpdb->prefix}adrotate
					JOIN {$wpdb->prefix}adrotate_linkmeta
					ON {$wpdb->prefix}adrotate.id = {$wpdb->prefix}adrotate_linkmeta.ad
					WHERE type = 'active' AND {$wpdb->prefix}adrotate_linkmeta.group = %d
					LIMIT 1
				",
				$id
			)
		);
	}

	function ad_group( $id ) {
		if ( $this->is_adrotate_enabled() ) {
			return adrotate_group( $id );
		}
	}

	private function is_adrotate_enabled () {
		return defined( 'ADROTATE_DISPLAY' );
	}
}

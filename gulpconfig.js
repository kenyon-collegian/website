module.exports = {
  development: {
    buildDir: "../public_html/wp-content/themes/third-floor-theme"
  },
  production: {
    buildDir: "./build"
  }
};

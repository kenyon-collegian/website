// Load plugins
const gulp = require("gulp");
const plumber = require("gulp-plumber");
const sourcemaps = require("gulp-sourcemaps");
const spritesmith = require("gulp.spritesmith");
const imagemin = require("gulp-imagemin");
const sass = require("gulp-sass");
const autoprefixer = require("gulp-autoprefixer");
const browserify = require("browserify");
const babelify = require("babelify");
const uglify = require("gulp-uglify");
const through2 = require("through2");
const buffer = require("vinyl-buffer");
const source = require("vinyl-source-stream");
const merge = require("merge-stream");
const browserSync = require("browser-sync").create();
const path = require("path");

// Get configuration constants
const config = require("./gulpconfig");

// Get current environment
const env = process.env.NODE_ENV || "development";

const buildDir = config[env].buildDir || "build";

// Configure source and destination pathname helpers
const fromSrc = (...args) => {
  return path.resolve("./src", ...args);
};
const toDest = (...args) => {
  return path.resolve(buildDir, ...args);
};

// Configure helper for making transformations only in dev environments
const onlyIfDev = (fn, ...args) => {
  return env === "development" ? fn(...args) : through2.obj();
};

// proxy the Apache virtual host
const startServer = () => {
  let browserSyncOpts = {
    proxy: {
      target: "kenyoncollegian.test",
      reqHeaders: {
        "x-dev-proxy": "1"
      }
    },
    ghostMode: false,
    online: true,
    reloadDebounce: 1500
  };
  return browserSync.init(browserSyncOpts);
};

// Transfer Timber template files
const moveTemplates = () => {
  return gulp
    .src(fromSrc("templates/**/*.twig"))
    .pipe(plumber())
    .pipe(gulp.dest(toDest("templates")))
    .pipe(browserSync.reload({ stream: true }));
};

// Transfer php files
const movePhp = () => {
  return gulp
    .src(fromSrc("**/*.php"))
    .pipe(plumber())
    .pipe(gulp.dest(toDest()))
    .pipe(browserSync.reload({ stream: true }));
};

// Minimize and transfer images
const buildImages = () => {
  return gulp
    .src(fromSrc("assets/images/*.(jpg|png|gif|svg)"))
    .pipe(plumber())
    .pipe(imagemin())
    .pipe(gulp.dest(toDest("assets/images")))
    .pipe(browserSync.reload({ stream: true }));
};

// Create a spritesheet and its corresponding stylesheet
const buildSpritesheet = () => {
  let spriteData = gulp
    .src(fromSrc("assets/sprites/*.png"))
    .pipe(plumber())
    .pipe(
      spritesmith({
        imgName: "sprite.png",
        cssName: "sprite-helpers.css"
      })
    );

  let imageStream = spriteData.img
    .pipe(buffer())
    .pipe(imagemin())
    .pipe(gulp.dest(toDest("assets")));

  let cssStream = spriteData.css
    .pipe(sass({ outputStyle: "compressed" }))
    .pipe(gulp.dest(toDest("assets")));

  return merge(imageStream, cssStream).pipe(
    browserSync.reload({ stream: true })
  );
};

// Transfer static assets
const moveStatic = () => {
  return gulp
    .src(fromSrc("assets/static/**/*"))
    .pipe(plumber())
    .pipe(gulp.dest(toDest("assets/static")))
    .pipe(browserSync.reload({ stream: true }));
};

// Favicon files
const moveFavicon = () => {
  return gulp
    .src(fromSrc("assets/favicon/*"))
    .pipe(plumber())
    .pipe(gulp.dest(toDest()))
    .pipe(browserSync.reload({ stream: true }));
};

// Compile Sass into main stylesheet
const buildStyles = () => {
  let sassOpts = {
    outputStyle: "compressed",
    includePaths: ["node_modules/"]
  };
  return gulp
    .src(fromSrc("assets/scss/**/*.scss"))
    .pipe(plumber())
    .pipe(onlyIfDev(sourcemaps.init))
    .pipe(sass(sassOpts))
    .pipe(autoprefixer())
    .pipe(onlyIfDev(sourcemaps.write, "./"))
    .pipe(gulp.dest(toDest()))
    .pipe(browserSync.stream({ match: "**/*.css" }));
};

// Uglify icon loading script
const buildIcons = () => {
  let browserifyOpts = {
    extensions: ["es6"]
  };

  return browserify(fromSrc("assets/js/icons.js"), browserifyOpts)
    .transform(babelify, { presets: ["env"] })
    .bundle()
    .pipe(source("icons.js"))
    .pipe(buffer())
    .pipe(plumber())
    .pipe(uglify())
    .pipe(gulp.dest(toDest("assets")))
    .pipe(browserSync.reload({ stream: true }));
};

// Bundle and uglify Javascript
const buildScripts = () => {
  let browserifyOpts = {
    debug: true,
    extensions: ["es6"]
  };

  return browserify(fromSrc("assets/js/main.js"), browserifyOpts)
    .transform(babelify, {
      presets: ["env"],
      plugins: [["transform-react-jsx", { pragma: "h" }]]
    })
    .bundle()
    .pipe(source("main.js"))
    .pipe(buffer())
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(uglify())
    .pipe(sourcemaps.write("./"))
    .pipe(gulp.dest(toDest("assets")))
    .pipe(browserSync.reload({ stream: true }));
};

const watch = () => {
  gulp.watch(fromSrc("**/*.php"), movePhp);
  gulp.watch(fromSrc("templates/**/*.twig"), moveTemplates);
  gulp.watch(fromSrc("assets/images/*.(jpg|png|gif|svg)"), buildImages);
  gulp.watch(fromSrc("assets/sprites/*.png"), buildSpritesheet);
  gulp.watch(fromSrc("assets/static/**/*"), moveStatic);
  gulp.watch(fromSrc("assets/scss/**/*.scss"), buildStyles);
  gulp.watch(
    fromSrc("assets/js/**/*.js"),
    gulp.parallel(buildIcons, buildScripts)
  );
  gulp.watch(fromSrc("assets/favicon/*"), moveFavicon);
};
watch.description = "Watch the source files for changes and rebuild as needed.";

const build = gulp.parallel(
  buildScripts,
  moveTemplates,
  movePhp,
  buildImages,
  buildSpritesheet,
  moveStatic,
  buildStyles,
  buildIcons,
  buildScripts,
  moveFavicon
);
build.description =
  "Collect the source files and build the Wordpress theme for distribution.";

const serve = gulp.series(build, gulp.parallel(startServer, watch));
serve.description = "Start a development server and rebuild as needed.";

module.exports = {
  build,
  buildIcons,
  buildImages,
  buildScripts,
  buildSpritesheet,
  buildStyles,
  moveFavicon,
  moveStatic,
  moveTemplates,
  serve,
  watch
};
